package rnids.dusan.com.whois;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionCheck {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static int getConnectivityStatusInt(Context context) {
        int conn = ConnectionCheck.getConnectivityStatus(context);
        int status = -1;
        if (conn == ConnectionCheck.TYPE_WIFI) {
            status = TYPE_WIFI;
        } else if (conn == ConnectionCheck.TYPE_MOBILE) {
            status = TYPE_MOBILE;
        } else if (conn == ConnectionCheck.TYPE_NOT_CONNECTED) {
            status = TYPE_NOT_CONNECTED;
        }
        return status;
    }
}
