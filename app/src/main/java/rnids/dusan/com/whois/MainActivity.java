package rnids.dusan.com.whois;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.StringReader;

import android.app.Activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.WindowManager;
import android.view.Display;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.webkit.JavascriptInterface;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;

public class MainActivity extends Activity {

    protected String query = null;
    protected String htmlData = null;

    //string constants used in MainActivity
    private static final String TAG = "WhoIs app class".concat(MainActivity.class.getSimpleName());
    private static final String HOME_WHOIS = "http://jobfairnis.rs/RNIDS/android.php";
    private static final String PHP_WHOIS_PART_URL = "jobfairnis.rs/RNIDS";

    /* Getter and setter methods for JavaScript interface returned strings */
    public void setQueryString(String query) {
        this.query = query;
    }

    public void sethtmlData(String htmlData) {
        this.htmlData = htmlData;
    }

    /* END of Getter and setter methods */

    private void createPdf(String filename, String htmlData) throws FileNotFoundException, DocumentException, IOException {
        //check htmlData string also!
        if (htmlData != null && !htmlData.isEmpty()) {

            File pdfFolder = new File(Environment.getExternalStorageDirectory(), "/WhoIs");

            if (!pdfFolder.exists()) {
                pdfFolder.mkdir();
                Log.i(TAG, "Pdf Directory created");
            }

            //save files to WhoIs directory in root SDCard external storage folder
            File myFile = new File(pdfFolder + "/" + "WhoIs_" + filename + ".pdf");

            FileOutputStream output = new FileOutputStream(myFile);

            //Step 1
            Document document = new Document();

            //PdfWriter instance
            PdfWriter.getInstance(document, output);

            //Open document for changes
            document.open();

            //add html data via HTMLWorker. I know it's deprecated, but works good with 'loose' HTML
            HTMLWorker htmlWorker = new HTMLWorker(document);
            htmlWorker.parse(new StringReader(htmlData));
            //Close and save document
            document.close();
        } else {
            Toast.makeText(getBaseContext(), "htmlData is null or empty", Toast.LENGTH_LONG).show();
        }

    }

    /*
        duka - get WebView to be force-aligned to device width
     */
    private int getScale() {
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point displaySize = new Point();
        display.getSize(displaySize);

        int width = displaySize.x;
        Double val = width / 800d;
        val = val * 100d;
        return val.intValue();
    }

    private void forceFixedViewport(WebView myWebView) {
        WebSettings settings = myWebView.getSettings();

        //set wide viewport and disable overview
        settings.setLoadWithOverviewMode(false);
        settings.setUseWideViewPort(true);

        //disable default zoom
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        //set scale
        int percentScale = getScale();
        myWebView.setInitialScale(percentScale);
    }

    //everyone uses this. but sometimes it won't work
    //so make this method just in case if needed in future
    private void normalFixedViewport(WebView myWebView) {
        WebSettings settings = myWebView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
    }
    /*
        End of WebView width magicianship :D
     */

    private class MyCustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //open phpWhoIs mobile view in current WebView
            if (url.contains(PHP_WHOIS_PART_URL)) {
                view.loadUrl(url);
                return true;
            } else {
                //open external link in browser if link root not same as phpWhoIs
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {

            int connectedNetworkCheck = ConnectionCheck.getConnectivityStatusInt(MainActivity.this);
            //redirect to no connection activity screen if no connected network
            if ((connectedNetworkCheck == 1) || (connectedNetworkCheck == 2)) {
                if (view.canGoBack()) {
                    view.goBack();
                }
                Toast toast = Toast.makeText(getBaseContext(), description,
                        Toast.LENGTH_SHORT);
                toast.show();
            } else {
                redirectToNoConn();
                finish();
            }

        }
    }


    public class AppJavaScriptProxy {

        private Activity activity = null;
        public String queryJS = null;

        public AppJavaScriptProxy(Activity activity) {
            this.activity = activity;
        }

        //gets query --> query is actually website URL/domain that's being searched
        @JavascriptInterface
        public void GetQuery(String query) {
            //set query string to current MainActivity instance
            this.queryJS = query;
            MainActivity.this.setQueryString(query);
        }

        //get html body data from WhoIs data result. JavaScript returns this var via interface to htmlData string
        @JavascriptInterface
        public void GethtmlData(String htmlData) {
            MainActivity.this.sethtmlData(htmlData);
        }

    }

    public void redirectToNoConn() {
        final Context context = this;
        Intent intent = new Intent(context, NoConnActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_pdf:
                if (query != null) {
                    Log.d(TAG, "MainActivity public String query = ".concat(query));
                    if (htmlData != null) {
                        try {
                            createPdf(query, htmlData);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(), "Error. Contact developer.", Toast.LENGTH_LONG).show();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(), "Error. Contact developer.", Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(), "Error. Contact developer.", Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(getBaseContext(), "Saved PDF to WhoIs folder in your external storage (SDCard) root", Toast.LENGTH_LONG).show();
                    } else {
                        Log.d(TAG, "MainActivity public String htmlData = null");
                    }
                } else {
                    Log.d(TAG, "MainActivity public String query = null");
                    String NoQueryMessageToast = "Enter Query and press 'WhoIs Query' button first";
                    Toast.makeText(getBaseContext(), NoQueryMessageToast, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = this;


        int connectedNetworkCheck = ConnectionCheck.getConnectivityStatusInt(context);
        if ((connectedNetworkCheck == 1) || (connectedNetworkCheck == 2)) {
            setContentView(R.layout.activity_main);

            WebView myWebView = (WebView) findViewById(R.id.webview);

            //settings check
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            //now force viewport to be aligned to device width
            //no more wider-than-screen web page
            forceFixedViewport(myWebView);
            myWebView.addJavascriptInterface(new AppJavaScriptProxy(this),
                    "WhoIsJS");

            myWebView.setWebViewClient(new MyCustomWebViewClient());
            myWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

            myWebView.loadUrl(HOME_WHOIS);
            return;
        } else {
            Toast.makeText(getBaseContext(), "No Internet connection", 1).show();
            redirectToNoConn();
            finish();
        }
    }

}