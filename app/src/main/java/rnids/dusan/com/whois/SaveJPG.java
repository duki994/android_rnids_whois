package rnids.dusan.com.whois;


import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

public class SaveJPG {
    //save context
    protected Context mContext;

    public SaveJPG (Context context) {
        this.mContext = context;
    }

    public boolean DownloadFile (String url, Context mContext) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
        DownloadManager manager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
        return true;
    }
}
