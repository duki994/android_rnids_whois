/**
 * Automatically generated file. DO NOT MODIFY
 */
package rnids.dusan.com.whois.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "rnids.dusan.com.whois.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.1";
}
