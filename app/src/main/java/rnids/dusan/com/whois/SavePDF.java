package rnids.dusan.com.whois;

import java.util.ArrayList;
import java.util.List;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.AsyncTask;
import java.io.IOException;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;



public class SavePDF {
    //save context
    protected Context mContext;
    private static final String TAG = SavePDF.class.getSimpleName();

    public SavePDF (Context context) {
        this.mContext = context;
    }

    public boolean DownloadFile (String url, Context mContext) {
        Log.d(TAG, "DownloadFile started");
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "name-of-the-file.ext");
        DownloadManager manager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
        return true;
    }

    //AsyncTask for download task
    private class MyAsyncTask extends AsyncTask<String, Integer, Double>{

        private String DownloadPDFurl;

        @Override
        protected Double doInBackground(String... params) {
            //Start POST request here
            postData(params[0]);
            return null;
        }

        protected void onPostExecute(Double result){
            //Download file now?
            DownloadFile(DownloadPDFurl, mContext);
        }

        public void postData(String QueryForPDF) {

            String SavePDFpostPHP = "http://jobfairnis.rs/RNIDS/SavePDF.php";

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(SavePDFpostPHP);
            try {

                List nameValuePairs = new ArrayList();

                nameValuePairs.add(new BasicNameValuePair("SavePDF", QueryForPDF));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                Log.d(TAG, "Sending POST request");
                HttpResponse response = httpclient.execute(httppost);

                DownloadPDFurl = SavePDFpostPHP.concat(" ");
                DownloadPDFurl = SavePDFpostPHP.concat(QueryForPDF);
            } catch (ClientProtocolException e) {
                Log.d(TAG, "ClientProtocolException");
            } catch (IOException e) {
                Log.d(TAG, "IOException");
            }
        }
    }
}
