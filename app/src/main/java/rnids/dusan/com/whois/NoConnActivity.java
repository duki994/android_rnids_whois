package rnids.dusan.com.whois;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.content.Intent;
import android.widget.Toast;

public class NoConnActivity extends Activity {

        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.no_conn_activity);

            Button myButton = (Button) findViewById(R.id.button);
            myButton.setOnClickListener(mGoListener);
        }

        private OnClickListener mGoListener = new OnClickListener()
        {
            public void onClick(View v)
            {
                //if not 0 it's connected to WiFi or Mobile Data
                if( ConnectionCheck.getConnectivityStatus(NoConnActivity.this) != 0) {
                    Intent intent = new Intent(NoConnActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "No Internet connection", 1).show();
                }

            }
        };

}
